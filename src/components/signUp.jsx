import { useState } from "react";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { spinner } from "../utils/spinner";
import { AlertTypesEnum } from '../utils/enums/alert.types.enum';

export const SignUp = (props) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });

  const [form, setForm] = useState({
    name: "",
    surnames: "",
    email: "",
    password: "",
    role: "",
  });

  const handleInputChange = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const saveUser = async (event) => {
    event.preventDefault();

    const _voidInputs = Object.entries(form).map(([key, value]) => {
      if (!value.trim()) {
        return key;
      }
    });

    const voidInputs = _voidInputs.filter((input) => input !== undefined);

    if (voidInputs.length === 0) {
      setLoading(true);

      const { status } = await axios.post(
        "http://localhost:3001/account/users",
        form
      );

      if (status === 201) {
        setLoading(false);
        props.history.push("/");
      } 
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };

  return (
    <div className="container">
      {loading ? (
        spinner()
      ) : (
        <div className="container">
          <div className="row">
            <div
              className="col-md-4"
              style={{ backgroundColor: "#bf3636" }}
            ></div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title mb-4">Registro de usuarios:</h5>
                {error ? handleAlert(alert.type, alert.key) : null}
                <form onSubmit={saveUser}>
                  <div className="form-content">
                    <div className="row">
                      <div className="form-group mb-2">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Nombre *"
                          name="name"
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="form-group mb-2">
                        <input
                          type="email"
                          className="form-control"
                          placeholder="Email *"
                          name="email"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Apellido *"
                          name="surnames"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Contraseña *"
                          name="password"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <select
                          className="form-control"
                          name="role"
                          onChange={handleInputChange}
                        >
                          <option value="">Seleccionar</option>
                          <option value="admin">Administrador</option>
                          <option value="receptionist">Funcionario</option>
                        </select>
                      </div>

                      <div class="form-group mb-2">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          style={{ marginRight: 4 }}
                          value=""
                          id="flexCheckDefault"
                        />
                        <label class="form-check-label" for="flexCheckDefault">
                          Acepta los{" "}
                          <a href="https://www.google.com">
                            Terminos y condiciones
                          </a>
                          .
                        </label>
                      </div>

                      <div className="d-grid gap-2">
                        <button className="btn btn-primary" type="submit">
                          Registrar
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
