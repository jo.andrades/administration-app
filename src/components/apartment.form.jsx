import { useState, useRef, useEffect } from "react";
import * as axios from "axios";
import { uploadFile } from "react-s3";
import { handleAlert } from "../utils/alerts";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";
import { spinner } from "../utils/spinner";
import jwt_decode from "jwt-decode";

export const ApartmentForm = (props) => {
  const s3_config = useRef({
    bucketName: "portfolio-jofas",
    dirName: "apartments" /* optional */,
    region: "sa-east-1",
    accessKeyId: "AKIA2MXPSWCJAF4I5TX2",
    secretAccessKey: "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",
  });
  const [user, setUser] = useState({ roles: [] });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      setUser(jwt_decode(token));
    } else {
      props.history.push("/");
    }
  }, []);

  const photoRef = useRef();
  const [photo, setPhoto] = useState(
    "https://portfolio-jofas.s3.sa-east-1.amazonaws.com/apartments/default-img.gif"
  );

  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });
  const [loading, setLoading] = useState(false);

  const [apartment, setApartment] = useState({
    region: "",
    address: "",
    roomsNum: "",
    bathroomNum: "",
    terrace: false,
    m2: "",
    maxCapacity: "",
    terms: "",
    isAvailable: false,
    isConditioned: false,
    floor: "",
    number: "",
  });

  const handlePhotoChange = (event) => {
    setLoading(true);
    const selectedPhoto = event.target.files[0];

    uploadFile(selectedPhoto, s3_config.current)
      .then(({ location }) => {
        setPhoto(location);
        setApartment({
          ...apartment,
          ["photo"]: location,
        });
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };

  const handleInputChange = (event) => {
    if (event.target.name === "terrace") {
      setApartment({
        ...apartment,
        [event.target.name]: event.target.checked,
      });
    } else if (
      event.target.name === "roomsNum" ||
      event.target.name === "bathroomNum" ||
      event.target.name === "m2" ||
      event.target.name === "maxCapacity"
    ) {
      setApartment({
        ...apartment,
        [event.target.name]: parseInt(event.target.value),
      });
    } else {
      setApartment({
        ...apartment,
        [event.target.name]: event.target.value,
      });
    }
  };

  const submitApartment = async (event) => {
    event.preventDefault();

    const _voidInputs = Object.entries(apartment).map(([key, value]) => {
      if (key !== "terrace" && key !== "photo") {
        if (!value.toString().trim()) {
          return key;
        }
      }
    });

    const voidInputs = _voidInputs.filter((input) => input !== undefined);

    if (voidInputs.length === 0) {
      const token = localStorage.getItem("accessToken");

      try {
        const { status } = await axios.post(
          `http://localhost:3008/administration/apartment`,
          apartment,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );

        if (status === 201) {
          props.history.push("/management");
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };

  return (
    <div className="container-fluid">
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
          Servicio Hotelero
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/management">
                Departamentos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/bookings">
                Reservas
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/register-apartment">
                Registro de departamentos
              </a>
            </li>
          </ul>
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={{ color: "white" }}
              >
                Hola, {user.name}!
              </span>
            </li>
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={{ color: "#FFD700" }}
              >
                {user.roles[0]}
              </span>
            </li>
          </ul>
        </div>
      </nav>

      <div className="container">
        <form onSubmit={submitApartment}>
          <div className="mb-3 mt-3 text-center">
            <h3>Registro Departamento</h3>
            <hr />
            {error ? handleAlert(alert.type, alert.key) : null}
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Región</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  name="region"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Dirección</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  name="address"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Número de piezas</div>
                </div>
                <input
                  type="number"
                  min="1"
                  className="form-control"
                  name="roomsNum"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Número de baños</div>
                </div>
                <input
                  type="number"
                  min="1"
                  className="form-control"
                  name="bathroomNum"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Metros cuadrados</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  name="m2"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Capacidad máxima</div>
                </div>
                <input
                  type="number"
                  min="1"
                  className="form-control"
                  name="maxCapacity"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Piso</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  name="floor"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Número dpto</div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  name="number"
                  onChange={handleInputChange}
                />
              </div>
              <div className="input-group mb-3 mr-sm-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Terminos</div>
                </div>
                <textarea
                  className="form-control"
                  name="terms"
                  rows="2"
                  onChange={handleInputChange}
                />
              </div>
              <div className="form-check form-switch">
                <input
                  className="form-check-input"
                  type="checkbox"
                  role="switch"
                  name="terrace"
                  defaultChecked={false}
                  onChange={handleInputChange}
                />
                <label className="form-check-label">Terraza</label>
              </div>
            </div>
            <div className="col-md-6">
              <div>
                <input
                  class="form-control form-control-lg"
                  name="photo"
                  ref={photoRef}
                  type="file"
                  onChange={handlePhotoChange}
                />
              </div>
              <hr />
              {loading ? (
                spinner()
              ) : (
                <>
                  <div class="text-center">
                    <img
                      style={{
                        borderRadius: "100%",
                        height: "auto",
                        width: "290px",
                      }}
                      src={photo}
                      class="rounded"
                      alt="..."
                    />
                  </div>
                  <br />
                  <div class="d-grid gap-2">
                    <button class="btn btn-primary" type="submit">
                      Guardar
                    </button>
                  </div>
                </>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};
