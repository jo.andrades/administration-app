import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";

export const Booking = (props) => {
  const [user, setUser] = useState({ roles: [] });
  const [bookings, setBookings] = useState({
    docs: [],
  });
  const [client, setClient] = useState({});
  const [apartment, setApartment] = useState({});

  const [bearerToken, setBearerToken] = useState("");

  const [detailMode, setDetailMode] = useState(false);
  const [endToday, setEndToday] = useState(false);
  const [checkinMode, setCheckinMode] = useState(false);
  const [checkOutMode, setCheckOutMode] = useState(false);

  const [selectedBooking, setSelectedBooking] = useState("");

  const [editForm, setEditForm] = useState({
    address: "",
    region: "",
    maxCapacity: "",
    terms: "",
    isAvailable: false,
    isConditioned: false,
  });
  const [reason, setReason] = useState("");
  const [totalFine, setTotalFine] = useState(0);
  const [isOk, setIsOk] = useState(false);

  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      getAllBookings(token, "startToday");
      setBearerToken(token);
      setUser(jwt_decode(token));
    } else {
      props.history.push("/");
    }
  }, []);

  const getCurrentIsoStringDate = () => {
    const today = new Date();
    today.setUTCHours(0, 0, 0, 0);
    return today.toISOString();
  };

  const getAllBookings = async (token, query) => {
    try {
      const date = getCurrentIsoStringDate();

      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3009/business/bookings/all-bookings?${query}=${date}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setBookings(data);
      // if (query === "endToday") {
      //   const filtered = bookings.docs.filter(
      //     (book) => book.checkIn.itsPaid === true && book.endAt === date
      //   );
      //   setBookings({ docs: filtered });
      // } else {
      //   setBookings(data);
      // }
    } catch (error) {
      console.log(error);
    }
  };

  const getClient = async (selectedClient) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3001/account/users/specific/${selectedClient}`,
        {
          headers: { Authorization: `Bearer ${bearerToken}` },
        }
      );
      setClient(data);
    } catch (error) {
      console.log(error);
    }
  };

  const confirmCheckin = async (selectedBooking) => {
    // /business/bookings/{_id}/confirm-checkin

    try {
      const {
        data: { data },
      } = await axios.put(
        `http://localhost:3009/business/bookings/${selectedBooking}/confirm-checkin`,
        {},
        {
          headers: { Authorization: `Bearer ${bearerToken}` },
        }
      );

      if (data) {
        const index = bookings.docs.findIndex(
          ({ _id }) => _id === selectedBooking
        );

        bookings.docs[index].bookingStatus = "in_progress";
        bookings.docs[index].checkIn.itsPaid = true;
        bookings.docs[index]["checkOut"] = { status: "pending" };

        setBookings({ ...bookings });
        console.log(bookings);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getApartment = async (selectedApartment) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3008/administration/apartment/${selectedApartment}`,
        {
          headers: { Authorization: `Bearer ${bearerToken}` },
        }
      );
      setApartment(data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleCheckin = () => {
    const book = bookings.docs.find(({ _id }) => _id === selectedBooking);

    return (
      <div>
        <h3 className="text-center">Info. CheckIn</h3>
        <hr />
        <div className="row">
          <div className="col-md-6">
            <label className="mb-3">
              Fecha de inicio: {book.startAt.substring(0, 10)}
            </label>
            <br />
            <label className="mb-3">Dpto: {apartment.region}</label>
            <br />
            <label className="mb-3">Piso: {apartment.floor}</label>
            <br />
            <label className="mb-3">Número: {apartment.number}</label>
            <br />
            <label className="mb-3">Dirección: {apartment.address}</label>
            <br />
            <label className="mb-3">
              Capacidad máxima:{" "}
              <strong style={{ color: "red" }}>{apartment.maxCapacity}</strong>{" "}
              personas
            </label>
          </div>
          <div className="col-md-6">
            <label className="mb-3">
              Fecha de fin: {book.endAt.substring(0, 10)}
            </label>
            {book.checkIn.itsPaid ? (
              <div>
                <br />
                <label>Estado pago 50% final: Completado</label>
              </div>
            ) : (
              <div>
                <br />
                <label>
                  Total 50% por pagar:{" "}
                  {Math.trunc(book.totalCost / 2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </label>
                <br />
                <button
                  type="button"
                  className="btn btn-primary mt-2"
                  onClick={() => {
                    confirmCheckin(book._id);
                  }}
                >
                  Verificar Pago
                </button>
              </div>
            )}
            {book.checkIn.persons.length > 0 ? (
              <div
                style={{
                  position: "relative",
                  height: "250px",
                  overflow: "auto",
                  display: "block",
                }}
              >
                <hr />
                <h5 className="mt-2">Acompañantes:</h5>
                <table className="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Contacto</th>
                      <th scope="col">DNI</th>
                    </tr>
                  </thead>
                  <tbody>
                    {book.checkIn.persons.map((person) => (
                      <tr key={person.email}>
                        <td>{person.name}</td>
                        <td>{person.phoneNumber}</td>
                        <td>{person.dni}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  };

  const handleDetail = () => {
    return (
      <div className="container-fluid">
        <br />
        <div className="row">
          <div className="col-md-3">
            <h3 className="text-center">Info. Cliente</h3>
            <hr />
            <div className="row">
              <div className="col-md-6">
                <label className="mb-3">Nombre: {client.name}</label>
                <label className="mb-3">Apellido: {client.surnames}</label>
                <label className="mb-3">Email: {client.email}</label>
              </div>
              <div className="col-md-6">
                <label className="mb-3">Rut: {client.dni}</label>
              </div>
              <div className="d-grid gap-2">
                <button
                  type="button"
                  className="btn btn-info"
                  onClick={() => {
                    setCheckOutMode(false);
                    setCheckinMode(true);
                  }}
                >
                  CheckIn
                </button>
                {endToday ? (
                  <button
                    type="button"
                    className="btn btn-warning"
                    onClick={() => {
                      setCheckinMode(false);
                      setCheckOutMode(true);
                    }}
                  >
                    CheckOut
                  </button>
                ) : null}
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={() => {
                    setDetailMode(false);
                    setCheckinMode(false);
                    setCheckOutMode(false);
                    setSelectedBooking("");
                  }}
                >
                  Cerrar
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-7">
            {checkinMode
              ? handleCheckin()
              : checkOutMode
              ? handleCheckOut()
              : null}
          </div>
        </div>
      </div>
    );
  };

  const handleInputChange = (event) => {
    if (event.target.name === "isOk") {
      setIsOk(event.target.checked);
    }
  };

  const handleCheckOut = () => {
    const book = bookings.docs.find(({ _id }) => _id === selectedBooking);

    return (
      <div>
        <h3 className="text-center">Info. CheckOut</h3>
        <hr />
        {error ? handleAlert(alert.type, alert.key) : null}
        <div className="row">
          <div className="col-md-6">
            <label className="mb-3">
              Fecha de inicio: {book.startAt.substring(0, 10)}
            </label>
            <br />
            <label className="mb-3">Dpto: {apartment.region}</label>
            <br />
            <label className="mb-3">Piso: {apartment.floor}</label>
            <br />
            <label className="mb-3">Número: {apartment.number}</label>
            <br />
            <label className="mb-3">Dirección: {apartment.address}</label>
            <label className="mb-3">
              Fecha de fin: {book.endAt.substring(0, 10)}
            </label>
            {book.checkIn.itsPaid ? (
              <div>
                <br />
                <label>Estado pago 50% final: Completado</label>
              </div>
            ) : (
              <div>
                <br />
                <label>
                  Total 50% por pagar:{" "}
                  {Math.trunc(book.totalCost / 2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </label>
                <br />
                <button
                  type="button"
                  className="btn btn-primary mt-2"
                  onClick={() => {
                    confirmCheckin(book._id);
                  }}
                >
                  Verificar Pago
                </button>
              </div>
            )}
          </div>
          <div className="col-md-6">
            {book.checkOut.status === "finished" ? (
              <h1 style={{ color: "red" }}>Checkout finalizado</h1>
            ) : (
              <div>
                <div className="form-check form-switch">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    role="switch"
                    name="isOk"
                    defaultChecked={isOk}
                    onChange={handleInputChange}
                  />
                  <label className="form-check-label">
                    Gastos extra por daños
                  </label>
                </div>
                <div>
                  {isOk ? (
                    <div>
                      <div className="mt-2">
                        <label htmlFor="">Razón: </label>
                        <textarea
                          className="form-control"
                          name="reason"
                          rows="2"
                          onChange={({ target: { value } }) => setReason(value)}
                        />
                      </div>
                      <div className="mt-2">
                        <label> Total multa: </label>
                        <input
                          type="text"
                          className="form-control"
                          name="totalFine"
                          onChange={({ target: { value } }) =>
                            setTotalFine(parseInt(value))
                          }
                        />
                      </div>
                      <button
                        type="submit"
                        className="btn btn-danger mt-3"
                        onClick={sendCheckOutWithConflict}
                      >
                        Reportar
                      </button>
                    </div>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={sendCheckOutWithOutConflict}
                    >
                      Finalizar
                    </button>
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  const sendCheckOutWithConflict = async () => {
    console.log(isOk);
    const voidInputs = [];

    if (isOk) {
      if (reason === "") {
        voidInputs.push("Razón");
      }
      if (totalFine === 0) {
        voidInputs.push("Total multa");
      }
    }

    if (voidInputs.length === 0) {
      try {
        const { status } = await axios.put(
          `http://localhost:3009/business/bookings/${selectedBooking}/reject-checkout`,
          {
            reason,
            totalFine,
          },
          {
            headers: { Authorization: `Bearer ${bearerToken}` },
          }
        );

        console.log(isOk);

        if (status === 200) {
          let book = bookings.docs.find(({ _id }) => _id === selectedBooking);

          book.bookingStatus = "finished";
          book.checkOut.status = "conflicts";

          const filtered = bookings.docs.filter(
            ({ _id }) => _id !== selectedBooking
          );

          filtered.push(book);

          setBookings({
            docs: filtered,
          });
          setDetailMode(false);
          setCheckOutMode(false);
          setTotalFine(0);
          setReason("");
          setIsOk(false);
          setError(false);
        }
      } catch (error) {}
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };

  const sendCheckOutWithOutConflict = async () => {
    try {
      const { status } = await axios.put(
        `http://localhost:3009/business/bookings/${selectedBooking}/confirm-checkout`,
        {},
        {
          headers: { Authorization: `Bearer ${bearerToken}` },
        }
      );

      setDetailMode(false);

      console.log(isOk);

      if (status === 200 && !isOk) {
        let book = bookings.docs.find(({ _id }) => _id === selectedBooking);

        book.bookingStatus = "finished";
        book.checkOut.status = "finished";

        const filtered = bookings.docs.filter(
          ({ _id }) => _id !== selectedBooking
        );

        filtered.push(book);

        setBookings({
          docs: filtered,
        });
      }
    } catch (error) {}
  };

  return (
    <div className="container-fluid">
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
          Servicio Hotelero
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/management">
                Departamentos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/bookings">
                Reservas
              </a>
            </li>
            {user.roles[0] === "admin" ? (
              <li className="nav-item">
                <a className="nav-link" href="/register-apartment">
                  Registro de departamentos
                </a>
              </li>
            ) : null}
          </ul>
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={{ color: "white" }}
              >
                Hola, {user.name}!
              </span>
            </li>
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={
                  user.roles[0] === "admin"
                    ? { color: "#FFD700" }
                    : { color: "aqua" }
                }
              >
                {user.roles[0]}
              </span>
            </li>
          </ul>
        </div>
      </nav>
      <div className="mb-3 mt-3">
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => {
            setBookings({ docs: [] });
            setDetailMode(false);
            setEndToday(false);
            setCheckOutMode(false);
            setCheckinMode(false);
            getAllBookings(bearerToken, "startToday");
          }}
        >
          Inician Hoy
        </button>
        &nbsp;
        <button
          type="button"
          className="btn btn-warning"
          onClick={() => {
            setBookings({ docs: [] });
            setDetailMode(false);
            setCheckOutMode(false);
            setCheckinMode(false);
            setEndToday(true);
            getAllBookings(bearerToken, "endToday");
          }}
        >
          Finalizan hoy
        </button>
      </div>
      {bookings.docs.length === 0 ? (
        <h1>No se encontraron reservas para este dia :c </h1>
      ) : (
        <div
          style={{
            position: "relative",
            height: "250px",
            overflow: "auto",
            display: "block",
          }}
        >
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                {endToday ? (
                  <th scope="col">Termina</th>
                ) : (
                  <th scope="col">Inicio</th>
                )}
                <th scope="col">Estado de la reserva</th>
                {endToday ? (
                  <th scope="col">Estado checkOut</th>
                ) : (
                  <th scope="col">Pago del 50% final</th>
                )}
                <th scope="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
              {bookings.docs.map((book) => (
                <tr key={book._id}>
                  {endToday ? (
                    <td>{book.endAt.substring(0, 10)}</td>
                  ) : (
                    <td>{book.startAt.substring(0, 10)}</td>
                  )}
                  <td>
                    {book.bookingStatus === "reserved" ? (
                      <div className="row">
                        <div
                          style={{
                            borderRadius: "50%",
                            width: "32px",
                            height: "32px",
                            background: "green",
                          }}
                        />{" "}
                        &nbsp; Reservado
                      </div>
                    ) : book.bookingStatus === "in_progress" ? (
                      <div className="row">
                        <div
                          style={{
                            borderRadius: "50%",
                            width: "32px",
                            height: "32px",
                            background: "blue",
                          }}
                        />{" "}
                        &nbsp; En curso
                      </div>
                    ) : book.bookingStatus === "cancelled" ? (
                      <div className="row">
                        <div
                          style={{
                            borderRadius: "50%",
                            width: "32px",
                            height: "32px",
                            background: "red",
                          }}
                        />{" "}
                        &nbsp; Cancelado
                      </div>
                    ) : book.bookingStatus === "pending" ? (
                      <div className="row">
                        <div
                          style={{
                            borderRadius: "50%",
                            width: "32px",
                            height: "32px",
                            background: "orange",
                          }}
                        />{" "}
                        &nbsp; Pendiente
                      </div>
                    ) : (
                      <div className="row">
                        <div
                          style={{
                            borderRadius: "50%",
                            width: "32px",
                            height: "32px",
                            background: "gray",
                          }}
                        />{" "}
                        &nbsp; Finalizado
                      </div>
                    )}
                  </td>
                  {endToday ? (
                    <td>
                      {book.checkOut.status === "pending" ? (
                        <div className="row">
                          <div
                            style={{
                              borderRadius: "50%",
                              width: "32px",
                              height: "32px",
                              background: "orange",
                            }}
                          />{" "}
                          &nbsp; Pendiente
                        </div>
                      ) : book.checkOut.status === "conflicts" ? (
                        <div className="row">
                          <div
                            style={{
                              borderRadius: "50%",
                              width: "32px",
                              height: "32px",
                              background: "red",
                            }}
                          />{" "}
                          &nbsp; Conflictos
                        </div>
                      ) : (
                        <div className="row">
                          <div
                            style={{
                              borderRadius: "50%",
                              width: "32px",
                              height: "32px",
                              background: "gray",
                            }}
                          />{" "}
                          &nbsp; Finalizado
                        </div>
                      )}
                    </td>
                  ) : (
                    <td>
                      {book.checkIn.itsPaid ? (
                        <div className="row">
                          <div
                            style={{
                              borderRadius: "50%",
                              width: "32px",
                              height: "32px",
                              background: "green",
                            }}
                          />{" "}
                          &nbsp; Pagado
                        </div>
                      ) : (
                        <div className="row">
                          <div
                            style={{
                              borderRadius: "50%",
                              width: "32px",
                              height: "32px",
                              background: "orange",
                            }}
                          />{" "}
                          &nbsp; Pendiente
                        </div>
                      )}
                    </td>
                  )}
                  <td>
                    <button
                      type="button"
                      className="btn btn-info"
                      onClick={() => {
                        setDetailMode(true);
                        setSelectedBooking(book._id);
                        getClient(book.client);
                        getApartment(book.apartment);
                      }}
                    >
                      Ver
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
      {detailMode ? handleDetail() : null}
    </div>
  );
};
