import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";

export const Management = (props) => {
  const [user, setUser] = useState({ roles: [] });
  const [apartments, setApartments] = useState({
    docs: [],
  });
  const [bearerToken, setBearerToken] = useState("");
  const [detailMode, setDetailMode] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [selectedApartment, setSelectedApartment] = useState("");

  const [editForm, setEditForm] = useState({
    address: "",
    region: "",
    maxCapacity: "",
    terms: "",
    isAvailable: false,
    isConditioned: false,
  });

  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      getAllApartments(token, true);
      setBearerToken(token);
      setUser(jwt_decode(token));
    } else {
      props.history.push("/");
    }
  }, []);

  const submitApartment = async (event) => {
    event.preventDefault();

    const _voidInputs = Object.entries(editForm).map(([key, value]) => {
      if (key !== "isAvailable" && key !== "isConditioned") {
        if (!value.toString().trim()) {
          return key;
        }
      }
    });

    const voidInputs = _voidInputs.filter((input) => input !== undefined);

    if (voidInputs.length === 0) {
      try {
        if (!editForm.isConditioned && editForm.isAvailable) {
          editForm.isAvailable = false;
        }

        console.log(editForm);

        const { status } = await axios.put(
          `http://localhost:3008/administration/apartment/${selectedApartment}`,
          editForm,
          {
            headers: { Authorization: `Bearer ${bearerToken}` },
          }
        );

        setEditMode(false);

        if ((status === 200 && !editForm.isAvailable) || editForm.isAvailable) {
          const filtered = apartments.docs.filter(
            ({ _id }) => _id !== selectedApartment
          );
          setApartments({
            docs: filtered,
          });
        } else {
          let edited = apartments.docs.find(
            ({ _id }) => _id === selectedApartment
          );

          edited = { ...edited, ...editForm };

          const filtered = apartments.docs.filter(
            ({ _id }) => _id !== selectedApartment
          );

          filtered.push(edited);

          setApartments({
            docs: filtered,
          });
        }
      } catch (error) {}
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };

  const handleInputChange = (event) => {
    if (
      event.target.name === "isAvailable" ||
      event.target.name === "isConditioned"
    ) {
      setEditForm({
        ...editForm,
        [event.target.name]: event.target.checked,
      });
    } else {
      setEditForm({
        ...editForm,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleDetail = () => {
    const apartment = apartments.docs.find(
      ({ _id }) => _id === selectedApartment
    );

    return (
      <div className="container-fluid">
        <br />
        <div className="row">
          <div className="col-md-4">
            <img
              style={{
                borderRadius: "8%",
                height: "300px",
                width: "300px",
              }}
              src={apartment.photo}
              alt=""
            />
          </div>
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-6">
                <label>Región: {apartment.region}</label>
                <hr />
                <label>Dirección: {apartment.address}</label>
                <hr />
                <label>Nro: {apartment.number}</label>
                <hr />
                <label>Piso: {apartment.floor}</label>
              </div>
              <div className="col-md-6">
                <label>Habitaciones: {apartment.roomsNum}</label>
                <hr />
                <label>Baños: {apartment.bathroomNum}</label>
                <hr />
                <label>
                  Capacidad máxima: {apartment.maxCapacity} personas
                </label>
                <hr />
                <label>Terminos: {apartment.terms}</label>
                <hr />
                <label>
                  Valor: $
                  {apartment.value
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                </label>
              </div>
            </div>
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => setDetailMode(false)}
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    );
  };

  const handleEdit = () => {
    const apartment = apartments.docs.find(
      ({ _id }) => _id === selectedApartment
    );

    return (
      <form onSubmit={submitApartment}>
        <div className="container-fluid">
          <h4 className="text-center">Editando...</h4>
          {error ? handleAlert(alert.type, alert.key) : null}
          <br />
          <div className="row">
            <div className="col-md-4">
              <img
                style={{
                  borderRadius: "8%",
                  height: "300px",
                  width: "300px",
                }}
                src={apartment.photo}
                alt=""
              />
            </div>
            <div className="col-md-6">
              <div className="row">
                <div className="col-md-6">
                  <div className="input-group mb-3 mr-sm-2">
                    <div className="input-group-prepend">
                      <div className="input-group-text">Dirección</div>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      name="address"
                      defaultValue={apartment.address}
                      onChange={handleInputChange}
                    />
                  </div>
                  <div className="input-group mb-3 mr-sm-2">
                    <div className="input-group-prepend">
                      <div className="input-group-text">Región</div>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      name="region"
                      defaultValue={apartment.region}
                      onChange={handleInputChange}
                    />
                  </div>
                  <div className="input-group mb-3 mr-sm-2">
                    <div className="input-group-prepend">
                      <div className="input-group-text">Capacidad</div>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      name="maxCapacity"
                      defaultValue={apartment.maxCapacity}
                      onChange={handleInputChange}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="input-group mb-3 mr-sm-2">
                    <div className="input-group-prepend">
                      <div className="input-group-text">Terminos</div>
                    </div>
                    <textarea
                      className="form-control"
                      name="terms"
                      rows="2"
                      defaultValue={apartment.terms}
                      onChange={handleInputChange}
                    />
                  </div>
                  <div className="form-check form-switch">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      role="switch"
                      name="isConditioned"
                      defaultChecked={apartment.isConditioned}
                      onChange={handleInputChange}
                    />
                    <label className="form-check-label">Acondicionado</label>
                  </div>
                  <div className="form-check form-switch">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      role="switch"
                      name="isAvailable"
                      defaultChecked={apartment.isAvailable}
                      onChange={handleInputChange}
                    />
                    <label className="form-check-label">Disponible</label>
                  </div>
                </div>
                <div>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => {
                      setEditMode(false);
                    }}
                  >
                    Cancelar
                  </button>
                  &nbsp;
                  <button type="submit" className="btn btn-warning">
                    Editar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  };

  const getAllApartments = async (token, status) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3008/administration/apartment?isAvailable=${status}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setApartments(data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container-fluid">
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
          Servicio Hotelero
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/management">
                Departamentos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/bookings">
                Reservas
              </a>
            </li>
            {user.roles[0] === "admin" ? (
              <li className="nav-item">
                <a className="nav-link" href="/register-apartment">
                  Registro de departamentos
                </a>
              </li>
            ) : null}
          </ul>
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={{ color: "white" }}
              >
                Hola, {user.name}!
              </span>
            </li>
            <li className="nav-item">
              <span
                className="nav-link"
                href="#"
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                style={
                  user.roles[0] === "admin"
                    ? { color: "#FFD700" }
                    : { color: "aqua" }
                }
              >
                {user.roles[0]}
              </span>
            </li>
          </ul>
        </div>
      </nav>
      <div className="mb-3 mt-3">
        <button
          type="button"
          className="btn btn-success"
          onClick={() => {
            setDetailMode(false);
            setEditMode(false);
            getAllApartments(bearerToken, true);
          }}
        >
          Disponibles
        </button>
        &nbsp;
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => {
            setDetailMode(false);
            setEditMode(false);
            getAllApartments(bearerToken, false);
          }}
        >
          No disponibles
        </button>
      </div>
      {apartments.docs.length === 0 ? (
        <h1>No se encontraron departamentos con este estado :c </h1>
      ) : (
        <div
          style={{
            position: "relative",
            height: "250px",
            overflow: "auto",
            display: "block",
          }}
        >
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">Dpto</th>
                <th scope="col">Dirección</th>
                <th scope="col">Estado</th>
                <th scope="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
              {apartments.docs.map((dpto) => (
                <tr key={dpto._id}>
                  <td>Dpto. {dpto.region}</td>
                  <td>{dpto.address}</td>
                  <td>
                    {dpto.isAvailable ? (
                      <div
                        style={{
                          borderRadius: "50%",
                          width: "32px",
                          height: "32px",
                          background: "green",
                        }}
                      ></div>
                    ) : (
                      <div
                        style={{
                          borderRadius: "50%",
                          width: "32px",
                          height: "32px",
                          background: "red",
                        }}
                      ></div>
                    )}
                  </td>
                  <td>
                    <button
                      type="button"
                      className="btn btn-info"
                      onClick={() => {
                        setDetailMode(true);
                        setEditMode(false);
                        setSelectedApartment(dpto._id);
                      }}
                    >
                      Ver
                    </button>
                    &nbsp;
                    {user.roles[0] === "admin" ? (
                      <button
                        type="button"
                        className="btn btn-warning"
                        onClick={() => {
                          const apartment = apartments.docs.find(
                            ({ _id }) => _id === dpto._id
                          );

                          setDetailMode(false);
                          setEditMode(true);
                          setSelectedApartment(dpto._id);
                          setEditForm({
                            address: apartment.address,
                            region: apartment.region,
                            maxCapacity: apartment.maxCapacity,
                            terms: apartment.terms,
                            isAvailable: apartment.isAvailable,
                            isConditioned: apartment.isConditioned,
                          });
                        }}
                      >
                        Editar
                      </button>
                    ) : null}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
      {detailMode ? handleDetail() : null}
      {editMode ? handleEdit() : null}
    </div>
  );
};

// setApartments({
//   docs: [
//     {
//       _id: "adadasd234sdf",
//       address: "Lomas #346",
//       apartmentNumber: "A35",
//       floor: 3,
//       region: "Los lagos",
//       roomsNum: 2,
//       bathroomNum: 1,
//       terrace: false,
//       m2: 200,
//       maxCapacity: 3,
//       terms: "Quis voluptate laboris consectetur et laborum et...",
//       isConditioned: true,
//       isAvailable: true,
//     },
//     {
//       _id: "adad235dd234sdf",
//       address: "Turbo #4238",
//       apartmentNumber: "E55",
//       floor: 6,
//       region: "Los rios",
//       roomsNum: 1,
//       bathroomNum: 1,
//       terrace: false,
//       m2: 200,
//       maxCapacity: 2,
//       terms: "No se aceptan perros",
//       isConditioned: true,
//       isAvailable: false,
//     },
//     {
//       _id: "adada52325sd234sdf",
//       address: "Av. Arturo Vidal #0947",
//       apartmentNumber: "79",
//       floor: 3,
//       region: "Araucania",
//       roomsNum: 4,
//       bathroomNum: 4,
//       terrace: false,
//       m2: 200,
//       maxCapacity: 8,
//       terms: "No hacer mucho ruido",
//       isConditioned: true,
//       isAvailable: false,
//     },
//     {
//       _id: "adadafsd245d234sdf",
//       address: "Av. las parcelas #45",
//       apartmentNumber: "35",
//       floor: 15,
//       region: "Metropolitana",
//       roomsNum: 3,
//       bathroomNum: 2,
//       terrace: false,
//       m2: 200,
//       maxCapacity: 4,
//       terms: "No se aceptan gatos",
//       isConditioned: true,
//       isAvailable: true,
//     },
//     {
//       _id: "adadasd5532sc234sdf",
//       address: "Av. colo colo #31",
//       apartmentNumber: "AA2",
//       floor: 2,
//       region: "Bio-Bio",
//       roomsNum: 1,
//       bathroomNum: 1,
//       terrace: false,
//       m2: 200,
//       maxCapacity: 2,
//       terms: "No se aceptan menores de edad",
//       isConditioned: true,
//       isAvailable: false,
//     },
//   ],
// });
