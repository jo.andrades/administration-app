import { SignUp } from "./components/signUp";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SignIn } from "./components/signIn";
import { Management } from "./components/management";
import { Booking } from "./components/booking";
import { ApartmentForm } from "./components/apartment.form";

function App() {
  return (
    <Router>
      <Switch>
        <Route component={ ApartmentForm } path="/register-apartment"/>
        <Route component={ Booking } path="/bookings"/>
        <Route component={ Management } path="/management"/>
        <Route component={ SignUp } path="/sign-up"/>
        <Route component={ SignIn } path="/"/>
      </Switch>
    </Router>
  );
}

export default App;
